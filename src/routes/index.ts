import * as express from "express";
import * as path from "path";
import * as bcrypt from "bcrypt";
import * as log4js from "log4js";
import * as jwt from "jsonwebtoken"; // used to create, sign, and verify tokens

import User, { IUser } from "../models/User";

// Init logger
const log = log4js.getLogger("route");

const SECRET = "HARD_SECRET";

interface MessageError {
	error: string;
	message: string;
}
interface MessageSuccess {
	message: string;
	id: string;
	username: string;
	email: string;
	token?: string;
}

const expressRouter: express.Router = express.Router();

// GET README page
expressRouter.get("/", (req, res, next) => {
	log.trace("Loading readme page...");
	res.status(200);
	return res.sendFile(path.join(__dirname, "../../README.md"));
});

// POST registration
expressRouter.post("/registration", (req: any, res, next) => {
	log.trace("Loading registration page...", req.body);
	// confirm that user typed same password twice
	if (req.body.password !== req.body.passwordConf) {
		const message: MessageError = {
			error: "invalid params",
			message: "Passwords do not match"
		};
		res.status(400);
		res.send(message);
	}

	if (req.body.email &&
		req.body.username &&
		req.body.password &&
		req.body.passwordConf) {

		const hashedPassword = bcrypt.hashSync(req.body.password, 8);

		const newUserModelData = {
			email: req.body.email,
			username: req.body.username,
			password: hashedPassword
		};

		// Create user in mongo through promis construction and use await
		helper.createUser(newUserModelData)

			.then((userModel) => {
				// if got model create answer for user
				if (userModel) {
					const messageRes: MessageSuccess = {
						message: "User created successfully",
						id: userModel.id,
						username: userModel.username,
						email: userModel.email
					};
					// 201 Created
					res.status(201).send(messageRes);
					return;
				}
			})
			// catch exceptions and check for error codes
			.catch((err) => {
				if (err.code === 11000) {
					const message: MessageError = {
						error: "duplicate error",
						message: "email is already registered"
					};
					// 400 Bad Request
					res.status(400).send(message);
					return;
				}
				return next(err);
			});

	} else {
		const message: MessageError = {
			error: "invalid request",
			message: "All fields required."
		};
		// 400 Bad Request
		res.status(400).send(message);
	}
});

// GET authorization
expressRouter.get("/authorization", (req: any, res, next) => {
	log.trace("Loading authorization page...", req.body);
	const email: string = req.body.email;
	const password: string = req.body.password;
	if (!(email && password)) {
		const message: MessageError = {
			error: "invalid request",
			message: "Email and password field required"
		};
		res.status(400).send(message);
	}

	// Find user in mongo model
	helper.findUser(email)

		.then((user) => {

			// If user is empty
			if (!user) {
				const message: MessageError = {
					error: "User not found.",
					message: "User doesn't exist"
				};
				res.status(401).send(message);
				return;
			}

			// Check password
			bcrypt.compare(password, user.password, (error: any, isCorrectPassword: boolean) => {
				if (error) {
					return next(error);
				}
				if (isCorrectPassword) {

					// if user is registered without errors
					// create a token
					const token = jwt.sign({ id: user._id }, SECRET, {
						expiresIn: 86400 // expires in 24 hours
					});

					const message: MessageSuccess = {
						message: "User authorization successfully",
						id: user.id,
						username: user.username,
						email: user.email,
						token
					};
					res.status(200).send(message);
				} else {
					const message: MessageError = {
						error: "invalid request",
						message: "Wrong email or password."
					};
					res.status(400).send(message);
				}
			});
		})

		.catch((err) => {
			return next(err);
		});
});

// GET inforation about user through jwt-token
expressRouter.get("/inforation", (req: any, res, next) => {
	log.trace("Loading inforation page...", req.headers);
	// check header or url parameters or post parameters for token
	const token = req.headers["x-access-token"];
	if (!token) {
		const message: MessageError = {
			error: "invalid request",
			message: "No token provided"
		};
		return res.status(403).send(message);
	}

	// verifies secret and checks exp
	helper.promisifyVerifyToken(token, SECRET)
		.then((decoded) => {
			const userId = decoded.id;

			// Find user by id
			helper.findUserById(userId)
				.then((user) => {
					if (user) {
						const message: MessageSuccess = {
							message: "User authorization successfully through jwt-token",
							id: user.id,
							username: user.username,
							email: user.email,
							token
						};
						return res.status(200).send(message);
					}
					return next();
				})
				.catch((err) => {
					return next(err);
				});
		})

		.catch((err) => {
			const message: MessageError = {
				error: err.name,
				message: err.message
			};
			// 401 Unauthorized
			return res.status(401).send(message);
		});
});

export namespace helper {
	export async function createUser(userModel: any) {
		const user = await promisifyCreateModel(User, userModel);
		/// validete ensure user
		return user;
	}
	export async function findUserById(id: string): Promise<IUser> {
		const userModel = await promisifyFindModelById(User, id);
		/// validete ensure userModel
		return userModel;
	}
	export async function findUser(email: string) {
		const userInfo = await promisifyFindOne(User, email);
		/// validete ensure userInfo
		return userInfo;
	}


	export function promisifyVerifyToken(token: string, secret: string): Promise<any> {
		return new Promise((resolve, reject) => {
			// verifies secret and checks exp
			jwt.verify(token, secret, function (err, decoded) {
				if (err) {
					return reject(err);
				}
				return resolve(decoded);
			});
		});
	}
	export function promisifyFindModelById(targetModel: any, id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			targetModel.findById(id)
				.exec(function (error: any, user: any) {
					if (error) {
						return reject(error);
					} else {
						resolve(user);
					}

				});
		});
	}
	export function promisifyCreateModel(targetModel: any, arg: any): Promise<any> {
		return new Promise((resolve, reject) => {
			targetModel.create(arg, function (error: any, user: any) {
				if (error) {
					return reject(error);
				} else {
					return resolve(user);

				}
			});
		});
	}
	export function promisifyFindOne(targetModel: any, arg: string): Promise<any> {
		return new Promise((resolve, reject) => {
			targetModel.findOne({ email: arg })
				.exec(function (err: any, user: any) {
					if (err) {
						reject(err);
					} else {
						resolve(user);
					}
				});

		});
	}
}

export default expressRouter;
