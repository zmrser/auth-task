import * as mongoose from "mongoose";

export interface IUser extends mongoose.Document {
	email: string;
	username: string;
	password: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	username: {
		type: String,
		required: true,
		trim: true
	},
	password: {
		type: String,
		required: true
	}
});

const User = mongoose.model<IUser>("User", UserSchema);
export default User;
