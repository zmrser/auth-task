import * as express from "express";
import * as http from "http";
import * as mongoose from "mongoose";
import * as bodyParser from "body-parser";
import * as log4js from "log4js";

import * as session from "express-session";
import * as connectMongo from "connect-mongo";
const MongoStore = connectMongo(session);

import routes from "./routes";

// init logger
const log = log4js.getLogger("main");
log.level = "debug";
log.trace("= TRACE IS ENABLED =");

log.info("Initializing application...");
const app = express();

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//connect to MongoDB
const dbname = process.env.NODE_ENV === "dev" ? "database-dev" : "database";
const dbpost = process.env.MONGO_INITDB_PORT || 27017;
const dbhost = process.env.MONGO_INITDB_HOST || "localhost";
const dbusername = process.env.MONGO_INITDB_ROOT_USERNAME || "";
const dbpassword = process.env.MONGO_INITDB_ROOT_PASSWORD || "";
mongoose.connect(`mongodb://${dbusername}:${dbpassword}@${dbhost}:${dbpost}/${dbname}`, {
	useCreateIndex: true,
	useNewUrlParser: true
});
const db = mongoose.connection;

//handle mongo error
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
	// we"re connected!
});

//use sessions for tracking logins
app.use(session({
	secret: "work hard",
	resave: true,
	saveUninitialized: false,
	store: new MongoStore({
		mongooseConnection: db
	})
}));

app.use(routes);

// 404 Not found (bad URL)
app.use(function (req: express.Request, res: express.Response): any {
	return res.status(404).end("404 Not Found");
});

// 5xx Fatal error
app.use(function (err: any, req: express.Request, res: express.Response): any {
	if (err) {
		//TODO: send email, log err, etc...
		log.error(err);
	}
	return res.status(500).end("500 Internal Error");
});

// Make HTTP server instance
const server = http.createServer(app);

// Register "listening" callback
server.on("listening", function (): any {
	const address = server.address();
	if (typeof address === "string") {
		log.info(`Server was started on ${address}`);
	} else {
		if (address) {
			log.info(address.family + " server was started on http://" + address.address + ":" + address.port);
		}
	}
});

const port: number = (process.env.PORT) ? parseInt(process.env.PORT) : 8080;
const listen: string = process.env.LISTEN || "localhost";

// Start listening
server.listen(port, listen);

log.info("Starting HTTP Web Server...");
