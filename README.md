# Auth task test

## POST Registration
Registration new user on Mongoose db and return message and status.	

Example use:
```JAVASCRIPT
Method: POST
URL: /registration
Body: x-www-form-urlencoded
parameters: 
{
email: serhii@gmail.com,
username: serhii,
password: qwe,
passwordConf: qwe
}
```
Success response (status code 200):
```
{
    "message": "User created successfully",
    "id": "5cf0e99072ef357d4d803226",
    "username": "serhii",
    "email": "serhii@gmail.com"
}
```
Error response (status code 400) // 500 something happened on server:
```
{
    error: "invalid request",
    message: "All fields required."
}
```

## GET authorization
Authorization user on application return message and status.

Example use:
```JAVASCRIPT
Method: GET
URL: /authorization
Body: x-www-form-urlencoded
parameters: 
{
email: serhii@gmail.com,
password: qwe
}
```
Success response (status code 200):
```
{
    "message": "User authorization successfully",
    "id": "5cf0e99072ef357d4d803226",
    "username": "serhii",
    "email": "serhii@gmail.com",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZjBlOTkwNzJlZjM1N2Q0ZDgwMzIyNiIsImlhdCI6MTU1OTI5MjM4NSwiZXhwIjoxNTU5Mzc4Nzg1fQ.EAJmdkawfjXf-Ge62eAkj1urjWrCTkusti2cGC2teHw"
}
```
Error response (status code 401) // 500 something happened on server:
```
{
    error: "User not found.",
    message: "User doesn't exist"
}
```

## GET inforation
Return inforation about user on application auth through jwt token.
Example use:
```JAVASCRIPT
Method: GET
URL: /authorization
Headers:
key: 
{
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZjBlOTkwNzJlZjM1N2Q0ZDMzIyNiIsImlhdCI6MTU1OTI5MjM4NSwiZXhwIjoxNTU5Mzc4Nzg1fQ.EAJmdkawfjXf-Ge62eAkj1urjWrCTkusti2cGC2teHw
}
```
Success response (status code 200):
```
{
    "message": "User authorization successfully through jwt-token",
    "id": "5cf0e64d3c6b051994ff9de3",
    "username": "serhii",
    "email": "serhii@gmail.com",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZjBlNjRkM2M2YjA1MTk5NGZmOWRlMyIsImlhdCI6MTU1OTI5ODAwNywiZXhwIjoxNTU5Mzg0NDA3fQ.Y23n1NkytHKU1_RU8T5sSytnLuKNPpEnRZmSZHNMGPU"
}
```
Error response (status code 401) // 500 something happened on server:
```
{
    error: "JsonWebTokenError",
    message: "invalid token"
}
```

# How run

Package installation:
```
npm install
```

Compile application:
```
npm run build
```

Run application:
```
npm start
```

Expectation:
```
Initializing application...
Starting HTTP Web Server...
IPv4 server was started on http://127.0.0.1:8080
```
