FROM node:10 as Builder

WORKDIR /build
COPY . .
RUN npm install --quiet
RUN npm run build

FROM node:10 as Packer
ARG TARGET
WORKDIR /pack
COPY --from=Builder /build/.dist/ usr/local/auth
COPY --from=Builder /build/README.md usr/local/auth
RUN cd usr/local/auth && npm install --production

FROM node:10
COPY --from=Packer /pack/ /
CMD ["node", "/usr/local/auth/lib/app.js"]
